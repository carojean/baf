# -*- coding: utf-8 -*-
import mimetypes
import logging

import magic
import sqlalchemy as sa

from .common import Common
from .meta import Base

log = logging.getLogger(__name__)


class FileType(Base, Common):
    __tablename__ = 'filetype'
    __table_args__ = (
        sa.UniqueConstraint('mimetype', 'magictype'),
        sa.Index('ix_filetype_mimetype_magictype',
                 'mimetype', 'magictype'),
    )

    id = sa.Column(sa.Integer, primary_key=True)
    mimetype = sa.Column(sa.Text, index=True)
    magictype = sa.Column(sa.Text, index=True)

    @classmethod
    def from_other(cls, dbsession, o):
        return dbsession.query(cls).filter(
            cls.mimetype == o.mimetype,
            cls.magictype == o.magictype
        ).first()

    @classmethod
    def init_from_filename(cls, path):
        """ Create an object from the path of a file on the local filesystem.
        """
        ft = cls()
        # Use standard Python mimetype
        # It does not need the content of the file, just the path.
        ft.mimetype, ft.encoding = mimetypes.guess_type(path, strict=False)
        # Use 'magic' (libmagic)
        # Note: could use from_buffer(open("testdata/test.pdf").read(1024))
        try:
            magictype = magic.from_file(path)
            if magictype:
                ft.magictype = magictype.split(',')[0]
                ft.typeinfo = magictype  # Not stored
            magicmime = magic.from_file(path, mime=True)
            if magicmime:
                if ft.mimetype is None:
                    ft.mimetype = magicmime
        except UnicodeDecodeError as exc:
            log.debug("%r", exc)
            pass
        return ft

    def __str__(self):
        return '%s %s' % (self.mimetype, self.magictype)

    def to_dict(self):
        d = super(FileType, self).to_dict()
        d.update(dict(
            mimetype=self.mimetype,
            magictype=self.magictype,
        ))
        return d
